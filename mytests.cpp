#include <gtest/gtest.h>
#include "range.cpp"

class RangeTest : public testing::Test {
  protected:
  SemiOpenRange *rng;

  void SetUp() {
    rng = new SemiOpenRange(2, 10);
  }
  void TearDown() {
    delete rng;
  }
};

TEST_F(RangeTest, length){
    int len = rng->length();
    ASSERT_EQ(len, 8);
}

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}