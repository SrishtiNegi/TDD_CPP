#include<iostream>
#include<cmath>
using namespace std;

class Range{
public:
	int high = 0, low = 0;
};

class SemiOpenRange : public Range{
public:
	SemiOpenRange(){
		this->low = 0;
		this->high = 0;
	}
	SemiOpenRange(int x){
		this->low = 0;
		this->high = x;
	}
	SemiOpenRange(int x, int y){
		this->low = x;
		this->high = y;
	}
    int length(){
		return this->high - this->low;
	}
};

